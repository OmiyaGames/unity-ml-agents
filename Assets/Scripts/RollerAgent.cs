﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;

[RequireComponent(typeof(Rigidbody))]
public class RollerAgent : Agent
{
    [Header("Target Stuff")]
    [SerializeField]
    private Transform target;
    [SerializeField]
    private Transform targetRangeMin;
    [SerializeField]
    private Transform targetRangeMax;

    [Header("Neato")]
    [SerializeField]
    private float speed = 5f;
    [SerializeField]
    private float distanceThreshold = 1.42f;

    private Rigidbody body;
    private Vector3 originalPosition;

    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody>();
        originalPosition = transform.position;
    }

    public override void AgentReset()
    {
        base.AgentReset();

        if(transform.position.y < 0)
        {
            body.angularVelocity = Vector3.zero;
            body.velocity = Vector3.zero;
            transform.position = originalPosition;
        }

        Vector3 position = target.position;
        position.x = Random.Range(targetRangeMin.position.x, targetRangeMax.position.x);
        position.y = Random.Range(targetRangeMin.position.y, targetRangeMax.position.y);
        position.z = Random.Range(targetRangeMin.position.z, targetRangeMax.position.z);
        target.position = position;
    }

    public override void CollectObservations()
    {
        base.CollectObservations();

        // Target and Agent position
        AddVectorObs(target.position);
        AddVectorObs(transform.position);

        // Agent velocities
        AddVectorObs(body.velocity.x);
        AddVectorObs(body.velocity.z);
    }

    public override void AgentAction(float[] vectorAction, string textAction)
    {
        base.AgentAction(vectorAction, textAction);

        // Apply the force on the body
        Vector3 control = new Vector3(vectorAction[0], 0, vectorAction[1]);
        body.AddForce(control * speed);

        // Get distance from the target
        float distance = Vector3.Distance(transform.position, target.position);
        if(distance < distanceThreshold)
        {
            // If within threshold, give an award
            SetReward(1.0f);
            Done();
        }
        else if(transform.position.y < 0)
        {
            Done();
        }
    }
}
